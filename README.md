# Hierachical Rank Pooling #


### What is this repository for? ###

This repository implements the Hierachical Rank Pooling method. Hierachical rank pooling is a sequence encoding method for human action and activity recognition. For more details please read our papers.
This is a matlab implementation.


### Dependency ###
Dependency : 
	Lib Linear 
      https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/multicore-liblinear/
	VL-Feat 
       http://www.vlfeat.org/
	LibSVM
     https://www.csie.ntu.edu.tw/~cjlin/libsvm/

### Papers ###

Fernando, Basura, et al. "Discriminative hierarchical rank pooling for activity recognition." Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2016.
http://www.cv-foundation.org/openaccess/content_cvpr_2016/papers/Fernando_Discriminative_Hierarchical_Rank_CVPR_2016_paper.pdf

@inproceedings{fernando2016discriminative,
  title={Discriminative hierarchical rank pooling for activity recognition},
  author={Fernando, Basura and Anderson, Peter and Hutter, Marcus and Gould, Stephen},
  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
  pages={1924--1932},
  year={2016}
}

Basura Fernando and Stephen Gould "Discriminatively Learned Hierarchical Rank Pooling Networks." International Journal of Computer Vision. 2017 
http://users.cecs.anu.edu.au/~basura/papers/HRP_IJCV.pdf


