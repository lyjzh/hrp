%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : 
% 1. Lib Linear 
%       https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/multicore-liblinear/
% 2. VL-Feat 
%       http://www.vlfeat.org/
% 3. LibSVM
%       https://www.csie.ntu.edu.tw/~cjlin/libsvm/
%
% Cite : 
% @inproceedings{fernando2016discriminative,
%  title={Discriminative hierarchical rank pooling for activity recognition},
%  author={Fernando, Basura and Anderson, Peter and Hutter, Marcus and Gould, Stephen},
%  booktitle={Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition},
%  pages={1924--1932},
%  year={2016}}
% @article{Fernando2017ijcv,
% Title = {Discriminatively Learned Hierarchical Rank Pooling Networks},
% Author = {Basura Fernando and Stephen Gould},
% journal = {International Journal of Computer Vision},
% volume = {},
% year = {2017},
% url = {},
% }
% 
% Author : Basura Fernando (basura.fernando@anu.edu.au)

function demo_resnet_classification()

CVAL    = 1;    % The defaul C value for SVR
WIN     = 20;   % The window size of the poooling layers
STRIDE  = 1;    % The stride of the pooling layer
% define the network architecture
net = defineNetwork(CVAL,WIN,STRIDE);   

% we are going to generate 1000 training and testing sequences
n_Trn = 1000;
n_Test = 500;

% GENERATING TRAINING SEQUENCE ENCODING
%

% place holder for the training labels
trn_labels = zeros(1,n_Trn);

% place holder for the trainig encoded sequences
encoded_trn_data = zeros(n_Trn,2048*3*16,'single');
fprintf('create train data\n');

for i = 1 : n_Trn
    % for each seqence load data and class label
    [sequence_data_resnet,trn_labels(i)] = getSeqData();
    % apply non-linear feature maps
    x = getNonLinearity(sequence_data_resnet,'chi1') ;
    % get the encoding of the sequence
    enc = passNetwork(x,net)  ;
    encoded_trn_data(i,:) =  single(enc);
    fprintf('.');if mod(i,100)==0, fprintf('\n'); end
end

% GENERATING TESTING SEQUENCE ENCODING
fprintf('create test data\n');
test_labels = zeros(1,n_Test);
encoded_test_data = zeros(n_Test,2048*3*16,'single');
for i = 1 : n_Test
    % for each seqence load data and class label
    [sequence_data_resnet,test_labels(i)] = getSeqData();
    % apply non-linear feature maps
    x = getNonLinearity(sequence_data_resnet,'chi1') ;
    % get the encoding of the sequence
    enc = passNetwork(x,net)  ;
    encoded_test_data(i,:) =  single(enc);
    fprintf('.');    if mod(i,100)==0, fprintf('\n'); end
end

dataset.classlabel = [trn_labels'; test_labels'];
dataset.traintest  = [ones(n_Trn,1) ; [ones(n_Test,1)+1]];


encoded_trn_data = normalizeL2(getNonLinearity(encoded_trn_data,'chi2exp')); 
encoded_test_data = normalizeL2(getNonLinearity(encoded_test_data,'chi2exp'));

TrainData_Kern = encoded_trn_data * encoded_trn_data';                
TestData_Kern = encoded_test_data * encoded_trn_data';

[multiclass,perclass] = getClassificationMeasures(dataset,TrainData_Kern,TestData_Kern)  ;

fprintf('Mean perclass accuracy %1.1f \n',mean(perclass));
fprintf('Multiclass  accuracy %1.1f \n',multiclass);


% dummy function that extracts ResNet152 features for a video sequnce
% create a sequence of vectors of dimensionality 2048 and return class
% label
function [sequence_data_resnet,label] = getSeqData()
    videoLenght = randi([50,100]);
    sequence_data_resnet = randn(videoLenght,2048,'single');
    label = randi([1,10]);

    
function [multiclass,perclass] = getClassificationMeasures(dataset,TrainData_Kern,TestData_Kern)  
    
    [classlabel]   = getLabel(dataset.classlabel);
    trn_indx       = find(dataset.traintest == 1);
    test_indx      = find(dataset.traintest ~= 1);
    TestClass      = classlabel(test_indx,:);
    TrainClass     = classlabel(trn_indx,:);
    test_classid   = dataset.classlabel(test_indx);
    trn_classid    = dataset.classlabel(trn_indx);
    score          = zeros(size(TestClass,1),size(classlabel,2));    
    cid = 1;
    CVAL = 100;
    for cl = 1 : size(classlabel,2)            
        trnLBLB = TrainClass(:,cl);
        testLBL = TestClass(:,cl);    
        [score(:,cl)] = getScores(TrainData_Kern,TestData_Kern,trnLBLB,testLBL,CVAL);                    
    end   
    
    [~,predcl] = max(score,[],2);        
    multiclass = numel(find(predcl==test_classid))/numel(test_classid) * 100;   
    
    for cl = 1 : size(score,2)     
        indx = find(test_classid ==cl);
        if numel(indx)> 0 && numel(find(trn_classid ==cl)) > 0
            perclass(cid) = numel(find (predcl(indx) == cl)) / numel(indx) * 100;            
            cid = cid + 1;
        end
    end   
    
    
    
function [X] = getLabel(classid)
    X = zeros(numel(classid),max(classid))-1;
    for i = 1 : max(classid)
        indx = find(classid == i);
        X(indx,i) = 1;
    end

function [score] = getScores(TrainData_Kern,TestData_Kern,TrainClass,TestClass,C)
        nTrain = 1 : size(TrainData_Kern,1);
        TrainData_Kern = [nTrain' TrainData_Kern];         
        nTest = 1 : size(TestData_Kern,1);
        TestData_Kern = [nTest' TestData_Kern];                         
        model = svmtrain(double(TrainClass), double(TrainData_Kern), sprintf('-t 4 -c %1.6f -q -b 1', C));
        [~, ~, scores] = svmpredict(double(TestClass), double(TestData_Kern) ,model, '-b 1');	                         
        score = scores(:,1);        

